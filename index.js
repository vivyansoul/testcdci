const calcs = (a, b) => {
  return a + b;
};

module.exports = {
  calcs,
};
