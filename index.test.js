const {calcs} = require("./index");

test("add 1 + 2 equal 3", () => {
  expect(calcs(1, 2)).toBe(3);
});
